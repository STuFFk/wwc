import React, { Component } from 'react';
import omit from 'lodash/omit';
import PropTypes from 'prop-types';

const addPostedContent = (WrappedComponent) => {
  return class AddPostedContent extends Component {
    static displayName = `AddPostedContent(${WrappedComponent.displayName || WrappedComponent.name})`;

    static propTypes = {
      onChange: PropTypes.func,
    };

    static defaultProps = {
      onChange(){},
    };

    constructor(props) {
      super(props);

      this.state = {
        typeId: '',
        userId: '',
        url: '',
      }
    }

    handleTypeChange = (event, data) => {
      this.setState(() => ({
        typeId: data.value,
      }), () => {
        this.handleChange();
      });
    };

    handleUserChange = (event, data) => {
      this.setState(() => ({
        userId: data.value,
      }), () => {
        this.handleChange();
      });
    };

    handleUrlChange = (event, data) => {
      this.setState(() => ({
        url: data.value,
      }), () => {
        this.handleChange();
      });
    };

    handleChange = () => {
      this.props.onChange(this.isSubmittable() ? this.state : null);
    };

    handleSubmit = () => {
      const { typeId, userId, url } = this.state;

      if (!this.isSubmittable()) {
        return;
      }

      this.props.onSubmit(userId, typeId, url);

      this.setState(() => ({
        typeId: '',
        userId: '',
        url: '',
      }));
    };

    isSubmittable() {
      const { typeId, userId, url } = this.state;
      return ( typeId && userId && url );
    }

    render() {
      return (
        <WrappedComponent
          isSubmittable={ this.isSubmittable() }

          onTypeChange={ this.handleTypeChange }
          onUserChange={ this.handleUserChange }
          onUrlChange={ this.handleUrlChange }
          onSubmit={ this.handleSubmit }

          {...this.state }
          {...omit(this.props, ['onSubmit']) }
        />
      );
    }
  }
};

export default addPostedContent;

