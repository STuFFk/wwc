import React, { Component } from 'react';

import { getTypeFromId } from '../services/getTypes';
import { getUserById, getTeamForUserId } from '../../teams';

const withPostedContentList = (firebaseContentCollectionName) => (WrappedComponent) => {
  return class WithAuthorizedUser extends Component {
    static displayName = `WithPostedContentList(${WrappedComponent.displayName || WrappedComponent.name})`;

    constructor(props) {
      super(props);

      this.state = {
        postedContentList: [],
      };

      const db = window.firebase.firestore();

      this.unsubscribe = db.collection(firebaseContentCollectionName)
        .orderBy('createdAt', 'desc')
        .onSnapshot(querySnapshot => {
          const postedContentList = [];

          querySnapshot.forEach(doc => {
            postedContentList.push(this.getPostedContentFromDoc(doc));
          });

          this.setState(() => ({ postedContentList }));
        });
    }

    getPostedContentFromDoc(doc) {
      const data = doc.data();
      const type = getTypeFromId(data.typeId);

      return {
        ...data,
        ...type,
        id: doc.id,
        user: getUserById(data.userId),
        team: getTeamForUserId(data.userId),
      }
    }

    componentWillUnmount() {
      this.unsubscribe();
    }

    render() {
      return <WrappedComponent postedContentList={ this.state.postedContentList } {...this.props } />;
    }
  };
};

export default withPostedContentList;
