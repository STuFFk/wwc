import find from 'lodash/find';
import {
  types,
} from '../constants';

export const getTypes = () => types;
export const getTypeFromId = (id) => find(types, ['id', id]);

export default getTypes;
