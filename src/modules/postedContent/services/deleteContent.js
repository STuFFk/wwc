import { FB_COLLECTION_CURRENT_POSTED_CONTENT } from '../constants';

const deleteContent = async (contentId) => {
  const db = window.firebase.firestore();
  await db.collection(FB_COLLECTION_CURRENT_POSTED_CONTENT).doc(contentId).delete();
};

export default deleteContent;
