import { FB_COLLECTION_CURRENT_POSTED_CONTENT } from '../constants';

const addContent = async (userId, typeId, url) => {
  const db = window.firebase.firestore();

  await db.collection(FB_COLLECTION_CURRENT_POSTED_CONTENT).add({
    userId,
    typeId,
    url,
    createdAt: Date.now(),
  });
};

export default addContent;
