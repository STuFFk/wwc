import React from 'react';
import { Select, Form, Input } from 'semantic-ui-react';

import addPostedContent from '../hoc/addPostedContent';
import { getUsers } from '../../teams';
import { getTypes } from '../../postedContent';

const AddPostedContentForm = ({
  onUserChange,
  onTypeChange,
  onUrlChange,
  typeId,
  userId,
  url,
}) => {
  const users = getUsers().map(user => ({
    key: user.id,
    text: user.displayName,
    value: user.id,
  }));

  const types = getTypes().map(type => ({
    key: type.id,
    text: `${type.name} (${type.points})`,
    value: type.id,
  }));

  return (
    <Form>
      <Form.Group widths='equal'>
        <Form.Field id='who' control={ Select } value={ userId } label='Who' options={ users } placeholder='Who' onChange={ onUserChange }/>
        <Form.Field id='what' control={ Select } value={ typeId } label='What' options={ types } placeholder='What' onChange={ onTypeChange }/>
      </Form.Group>

      <Form.Field id='where' control={ Input } value={ url } label='Where' placeholder='https://' onChange={ onUrlChange }/>

      {/*<Form.Field>*/}
        {/*<Button primary disabled={ !isSubmittable } onClick={ onSubmit }>Add</Button>*/}
      {/*</Form.Field>*/}
    </Form>
  )
};

export default addPostedContent(AddPostedContentForm);
