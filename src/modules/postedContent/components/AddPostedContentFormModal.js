import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';

// import { Select, Form, Input, TextArea, Button } from 'semantic-ui-react';
//
import AddPostedContentForm from './AddPostedContentForm';
// import { getUsers } from '../../teams';
// import { getTypes } from '../../postedContent';

class AddPostedContentFormModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      postedContent: null,
    }
  }

  handleChange = (postedContent) => {
    this.setState(() => ({
      postedContent,
    }))
  };

  handleSubmit = () => {
    this.props.onSubmit(this.state.postedContent);
    this.props.onClose();
  };

  render() {
    const { isOpened, onClose } = this.props;
    const isSubmitable = this.state.postedContent;

    return (
      <Modal
        onClose={ onClose }
        dimmer="blurring"
        closeOnDimmerClick={ true }
        open={ isOpened }
        size='fullscreen'
      >
        <Header icon='list layout' content='Add something' />
        <Modal.Content>
          <AddPostedContentForm onChange={ this.handleChange }/>
        </Modal.Content>
        <Modal.Actions>
          <Button primary disabled={ !isSubmitable } onClick={ this.handleSubmit }>
            <Icon name='add square' /> Add
          </Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

// = ({ isOpened, onClose, onSubmit }) => {
//   const handleSubmit = (userId, typeId, url) => {
//     onSubmit(userId, typeId, url);
//     onClose();
//   };


// }

export default AddPostedContentFormModal;
