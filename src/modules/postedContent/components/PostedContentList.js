import React from 'react';
import PropTypes from 'prop-types';
import { Table, Icon, Button } from 'semantic-ui-react';

const propTypes = {
  onAdd: PropTypes.func,
  onDelete: PropTypes.func,
  readOnly: PropTypes.bool,
};

const defaultProps = {
  onAdd(){},
  onDelete(){},
  readOnly: false,
};

const CurrentPostedContentList = ({ postedContentList, onAdd, onDelete, readOnly }) => {
  if (postedContentList.length === 0) {
    return <div style={ { padding: '20px' } }>loading...</div>
  }

  return (
    <Table celled striped>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell singleLine>Who</Table.HeaderCell>
          <Table.HeaderCell singleLine>What</Table.HeaderCell>
          <Table.HeaderCell singleLine>Points</Table.HeaderCell>
          <Table.HeaderCell colSpan='2' singleLine>When</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        { !readOnly &&
          <Table.Row>
            <Table.Cell colSpan='5' textAlign="center">
              <Button icon="add square" primary content="Add something" onClick={ onAdd }/>
            </Table.Cell>
          </Table.Row>
        }

        {
          postedContentList.map(content => {
            const handleDelete = () => {
              onDelete(content);
            };

            return (
              <Table.Row key={content.id}>
                <Table.Cell><Icon circular inverted color={content.team.color} name='users'/> {content.user.displayName}
                </Table.Cell>
                <Table.Cell><a href={content.url} target="_blank">{content.name}</a></Table.Cell>
                <Table.Cell>{content.points}</Table.Cell>
                <Table.Cell>{new Date(content.createdAt).toLocaleDateString()}</Table.Cell>
                { !readOnly &&
                  <Table.Cell textAlign="center"><Button circular icon='trash' color='red' onClick={handleDelete}/></Table.Cell>
                }
              </Table.Row>
            )
          })
        }
      </Table.Body>
    </Table>
  );
};

CurrentPostedContentList.propTypes = propTypes;
CurrentPostedContentList.defaultProps = defaultProps;

export default CurrentPostedContentList;
