import { FB_COLLECTION_CURRENT_POSTED_CONTENT } from '../constants';

import PostedContentList from './PostedContentList';
import withPostedContentList from '../hoc/withPostedContentList';

export default withPostedContentList(FB_COLLECTION_CURRENT_POSTED_CONTENT)(PostedContentList);
