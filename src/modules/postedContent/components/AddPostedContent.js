import React, { Component } from 'react';
import { Button, Dropdown, Menu, Input } from 'semantic-ui-react';

import getTypes from '../services/getTypes';
import addContent from '../services/addContent';
import { getUsers } from '../../teams';

class AddPostedContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      typeId: null,
      userId: null,
      url: null,
    }
  }

  handleTypeChange = (event, data) => {
    this.setState(() => ({
      typeId: data.value,
    }));
  };

  handleUserChange = (event, data) => {
    this.setState(() => ({
      userId: data.value,
    }));
  };

  handleUrlChange = (event, data) => {
    this.setState(() => ({
      url: data.value,
    }));
  };

  handleSubmit = () => {
    const { typeId, userId, url } = this.state;

    if (!this.isSubmittable()) {
      return;
    }

    addContent(userId, typeId, url);

    this.setState(() => ({
      typeId: null,
      userId: null,
      url: null,
    }));
  };

  isSubmittable() {
    const { typeId, userId, url } = this.state;
    return ( typeId && userId && url );
  }

  render() {
    const isSubmittable = this.isSubmittable();

    return (
      <Menu borderless>
        <Menu.Menu>
          <Dropdown
            onChange={ this.handleUserChange }
            item
            placeholder='Author'
            search
            options={ getUsers().map(user => ({ key: user.id, value: user.id, text: user.displayName })) }
          />
        </Menu.Menu>

        <Menu.Item><strong>produced a</strong></Menu.Item>

        <Menu.Menu>
          <Dropdown
            onChange={ this.handleTypeChange }
            item
            placeholder='Type'
            options={ getTypes().map(type => ({ key: type.id, value: type.id, text: type.name })) }
          />
        </Menu.Menu>

        <Menu.Item><strong>here</strong></Menu.Item>

        <Menu.Item style={ { flexGrow: 1 } }>
          <Input onChange={ this.handleUrlChange } placeholder='https://' />
        </Menu.Item>

        <Menu.Item position="right">
          <Button primary onClick={ this.handleSubmit } disabled={ !isSubmittable }>Add</Button>
        </Menu.Item>
      </Menu>
    );
  }
}

export default AddPostedContent;

