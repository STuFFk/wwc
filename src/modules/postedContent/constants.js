export const TYPES_ID_SLACK = 1;
export const TYPES_ID_INTERNAL_KEYNOTE = 2;
export const TYPES_ID_BLOG_POST = 3;
export const TYPES_ID_NON_TECH_BLOG_POST = 7;
export const TYPES_ID_INTERNAL_OPENED_KEYNOTE = 4;
export const TYPES_ID_EXTERNAL_PUBLIC_KEYNOTE = 5;
export const TYPES_ID_TESTIMONIAL = 6;

export const FB_COLLECTION_CURRENT_POSTED_CONTENT = 'postedContent2';
export const FB_COLLECTION_PREVIOUS_POSTED_CONTENT = 'postedContent';

export const types = [
  {
    id: TYPES_ID_SLACK,
    name: 'Slack Post',
    points: 1,
  },
  {
    id: TYPES_ID_INTERNAL_KEYNOTE,
    name: 'Internal tech keynote',
    points: 10,
  },
  {
    id: TYPES_ID_NON_TECH_BLOG_POST,
    name: 'Non Tech Blog Post',
    points: 15,
  },
  {
    id: TYPES_ID_BLOG_POST,
    name: 'Blog Post',
    points: 60,
  },
  {
    id: TYPES_ID_INTERNAL_OPENED_KEYNOTE,
    name: 'Internal keynote opened to ext',
    points: 60,
  },
  {
    id: TYPES_ID_EXTERNAL_PUBLIC_KEYNOTE,
    name: 'External public keynote',
    points: 120,
  },
  {
    id: TYPES_ID_TESTIMONIAL,
    name: 'Testimonial',
    points: 200,
  },
];
