import React, { Component } from 'react';
import { Header, Menu } from 'semantic-ui-react'

import { User } from '../../../auth';

class AppBar extends Component {
  render() {
    return (
      <Menu inverted borderless>
        <Menu.Item>
          <Header as='h1' size="medium" inverted>World Wide Conqueror</Header>
        </Menu.Item>
        <Menu.Item position='right'>
          <User />
        </Menu.Item>
      </Menu>
    );
  }
}

export default AppBar;
