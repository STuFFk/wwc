import { getEmails } from '../../teams';

const canLog = (email) => getEmails().includes(email);

export default canLog;
