import React, { Component } from 'react';

class SignIn extends Component {
  componentDidMount() {
    window.firebase.auth().onAuthStateChanged(user => {
      if (!user) {
        const ui = new window.firebaseui.auth.AuthUI(window.firebase.auth());
        ui.reset();
        ui.start(this.rootElement, this.getUiConfig());
      }
    });
  }

  getUiConfig() {
    return {
      'callbacks': {
        // Called when the user has been successfully signed in.
        'signInSuccess': function(user, credential, redirectUrl) {
          console.log(user);
          // handleSignedInUser(user);
          // // Do not redirect.
          return false;
        }
      },
      'signInFlow': 'popup',
      'signInOptions': [
        {
          provider: window.firebase.auth.GoogleAuthProvider.PROVIDER_ID,
          scopes: ['https://www.googleapis.com/auth/plus.login']
        }
      ],
      // Terms of service url.
      'tosUrl': '/'
    };
  }

  onMount = (element) => {
    if (!element) {
      return;
    }

    this.rootElement = element;
  };

  render() {
    return <div ref={ this.onMount } />;
  }
}

export default SignIn;
