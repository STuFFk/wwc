import React, { Component } from 'react';
import { Image } from 'semantic-ui-react';
import SignIn from '../../../../modules/auth/components/SignIn';

// import Avatar from 'material-ui/Avatar';

class User extends Component {
  constructor(props) {
    super(props);

    this.state = {
      displayName: null,
      email: null,
      photoURL: null,
    }
  }

  componentDidMount() {
    window.firebase.auth().onAuthStateChanged(user => {
      if (!user) {
        return;
      }

      const { displayName, email, photoURL } = user;

      this.setState(() => ({
        displayName,
        email,
        photoURL,
      }));
    });
  }

  handleLogOut = () => {
    window.firebase.auth().signOut();
  };

  render() {
    if (!this.state.displayName) {
      return <SignIn />;
    }

    return (
      <div>
        <Image src={ this.state.photoURL } size="mini" avatar />
      </div>
    );
  }
}

export default User;
