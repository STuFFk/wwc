import React, { Component } from 'react';

import canLog from '../services/canLog';

const withAuthorizedUser = (WrappedComponent) => {
  return class WithAuthorizedUser extends Component {
    static displayName = `WithAuthorizedUser(${WrappedComponent.displayName || WrappedComponent.name})`;

    constructor(props) {
      super(props);

      this.state = {
        displayName: null,
        email: null,
        photoURL: null,
      }
    }

    componentDidMount() {
      window.firebase.auth().onAuthStateChanged(user => {
        if (!user) {
          this.setState(() => ({
            displayName: null,
            email: null,
            photoURL: null,
          }));
        } else {
          const { displayName, email, photoURL } = user;

          this.setState(() => ({
            displayName,
            email,
            photoURL,
          }));
        }
      });
    }

    render() {
      const { displayName, email, photoURL } = this.state;
      const userCanLog = canLog(email);
      const user = (displayName && userCanLog) ? { displayName, email, photoURL } : null;

      return <WrappedComponent user={ user } {...this.props } />;
    }
  };
};

export default withAuthorizedUser;
