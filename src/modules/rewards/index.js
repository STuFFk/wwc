import sortBy from 'lodash/sortBy';

export const PRICEPOOL = 1000;
export const TEAM_REWARD_PERCENT = 0.5;
export const USERS_REWARD_PERCENT = 1.0 - TEAM_REWARD_PERCENT;
export const USERS_TO_REWARD = 5;

export const getUserReward = (usersScore, teamsScore) => {
  const sorted = sortBy(usersScore, 'score').reverse();
  const winningTeam = getWinningTeam(teamsScore);

  const userRewardedScores = sorted.slice(0, USERS_TO_REWARD);
  const pointsOfRewaredUsers = userRewardedScores.reduce((total, { score }) => total + score, 0);
  const percentList = sorted.slice(0, 5).reduce((percentList, { user, score }) => {
    percentList[user.id] = score / (pointsOfRewaredUsers / 100) / 100;
    return percentList;
  }, {});

  return sorted.map((data, n) => {
    const userIsInWinningTeam = winningTeam.members.includes(data.user.id);
    const teamReward = Math.round(userIsInWinningTeam ? ((PRICEPOOL * TEAM_REWARD_PERCENT) / winningTeam.members.length) : 0);
    const userReward = Math.round(((n < USERS_TO_REWARD) ? percentList[data.user.id] * (PRICEPOOL * USERS_REWARD_PERCENT) : 0));

    return {
      ...data,
      teamReward: teamReward,
      userReward: userReward,
      reward: teamReward + userReward,
    }
  });
};

export const getWinningTeam = (teamsScore) => {
  let scores = teamsScore;

  // skip last column, which is column for people exluded from the contest
  scores.pop();

  // sort by score ASC
  scores = sortBy(scores, 'score');

  return scores.pop();
};
