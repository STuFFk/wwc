import find from 'lodash/find';
import upperFirst from 'lodash/upperFirst';

export { default as withTeamScores } from './hoc/withTeamScores';
export { default as TeamsPreviousScore } from './components/TeamsPreviousScore';
export { default as TeamsCurrentScore } from './components/TeamsCurrentScore';
export { default as TeamsMembers } from './components/TeamsMembers';
export { default as Winner } from './components/Winner';
export { default as PeopleReward } from './components/PeopleReward';

let users = [];

let teams = [
  { id: 1, color: 'red', name: 'Team 1', members: [1, 2, 3, 4, 5, 6, 25] },
  { id: 2, color: 'teal', name: 'Team 2', members: [7, 8, 9, 10, 11, 12, 23] },
  { id: 3, color: 'purple', name: 'Team 3', members: [13, 14, 15, 16, 17, 22, 21] },
  { id: 4, color: 'blue', name: 'Team 4', members: [18, 20, 24] },
];

export const productTeamId = 4;

export const getUsers = () => users;
export const setUsers = (newUsers) => { users = newUsers };
export const getUserById = (id) => find(getUsers(), ['id', id]);
export const getTeamForUserId = (id) => find(teams, (team) => team.members.includes(id));
export const getTeams = () => teams;
export const setTeams = (newTeams) => { teams = newTeams };
export const getEmails = () => getUsers().map(user => user.email).sort();
export const getDisplayNameFromEmail = email => upperFirst(email.match(/^(.*?)\./)[1]);

export const fetchUsers = async () => {
  const querySnapshot = await window.firebase.firestore()
    .collection('users')
    .orderBy('email', 'asc')
    .get();

  return querySnapshot.docs.map(doc => {
    const { email } = doc.data();
    return {
      id: +doc.id,
      displayName: getDisplayNameFromEmail(email),
      email,
    }
  });
};
