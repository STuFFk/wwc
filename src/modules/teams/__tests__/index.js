import { getEmails, getUsers, setUsers, setTeams, getTeamForUserId, getDisplayNameFromEmail } from '../';


describe('teams', () => {
  beforeEach(() => {
    setUsers([
      { id: 1, email: 'stephane.dupond@email.com' },
      { id: 2, email: 'alexandre.plop@email.com' },
    ]);

    setTeams([
      { id: 1, name: 'Team1', members: [1] },
      { id: 2, name: 'Team2', members: [2] },
    ]);
  });

  describe('#getEmails', () => {
    it('should return all emails, sorted alphabaetically', () => {
      const emails = getEmails();
      expect(Array.isArray(emails)).toBeTruthy();
      expect(emails.length).toEqual(getUsers().length);
      expect(emails[0]).toEqual(getUsers()[1].email);
    });
  });

  describe('#getUsers', () => {
    let users;

    beforeEach(() => {
      users = getUsers();
    });

    it('should return all users, sorted by last name', () => {
      expect(Array.isArray(users)).toBeTruthy();
    });

    it('should return display name from email', () => {
      expect(getDisplayNameFromEmail(users[1].email)).toEqual('Alexandre');
    });
  });

  describe('#getTeamForUserId', () => {
    it('should return team for a user', () => {
      expect( getTeamForUserId(1).id).toEqual(1);
      expect( getTeamForUserId(2).id).toEqual(2);
    });
  });
});



