import React from 'react';
import sortBy from 'lodash/sortBy';
import map from 'lodash/map';
import injectSheet from 'react-jss'
import { Card, Table } from 'semantic-ui-react';

import { getUserReward } from '../../rewards';
import withTeamScores from '../hoc/withTeamScores';
import { FB_COLLECTION_PREVIOUS_POSTED_CONTENT } from '../../postedContent';
import { getUserById, getTeamForUserId, productTeamId } from '..';

const styles = {
  // root: { textAlign: 'center' },
  // cup: {
  //   fontSize: '4rem',
  //   lineHeight: '4rem',
  //   marginBottom: '1rem',
  // },
  // title: {
  //   fontSize: '1.5rem',
  // }
};

const PeopleReward = ({ classes, usersScore, teamsScore, ignoreProductColumn }) => {
  if (!usersScore) {
    return false;
  }

  let scores = map(usersScore, ({ score }, userId) => {
    const id = +userId;
    return {
      user: getUserById(id),
      team: getTeamForUserId(id),
      score,
    }
  });

  scores = scores.filter((score) => {
    return score.team.id !== productTeamId;
  });

  scores = getUserReward(scores, teamsScore);

  scores = sortBy(scores, 'score')
    .reverse()
    .filter((score) => score.reward !== 0);

  console.log(scores);
  return (
    <Card fluid raised>
      <Card.Content header='Reward per member' />
      <Card.Content>
        <Table celled striped unstackable={ true }>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Score</Table.HeaderCell>
              <Table.HeaderCell>Team reward</Table.HeaderCell>
              <Table.HeaderCell>User reward</Table.HeaderCell>
              <Table.HeaderCell>Total reward</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            { scores.map(({ user, score, reward, teamReward, userReward }) => {
              return (
                <Table.Row key={ user.id }>
                  <Table.Cell singleLine>{ user.displayName }</Table.Cell>
                  <Table.Cell singleLine>{ score ? `${score} pts` : '-' }</Table.Cell>
                  <Table.Cell singleLine>{ teamReward ? `${teamReward} €` : '-' }</Table.Cell>
                  <Table.Cell singleLine>{ userReward ? `${userReward} €` : '-' }</Table.Cell>
                  <Table.Cell singleLine>{ reward } €</Table.Cell>
                </Table.Row>
              )
            })
            }
          </Table.Body>
        </Table>

      </Card.Content>
    </Card>
  );

  // let scores = teamsScore;

  // sort by score ASC
  // scores = sortBy(scores, 'score');

  // if (!winner) {
  //   return null;
  // }

  // let members = winner.members.map((memberId) => {
  //   const user = getUserById(memberId);
  //   const userScore = usersScore[memberId] || { score : 0};
  //   return {
  //     user,
  //     score: userScore.score,
  //   }
  // });
  //
  // members = sortBy(members, 'score').reverse();

  // return (
  //   <Card fluid raised>
  //     <Card.Content header='Score by team members' />
  //     <Card.Content>
  //       <Table celled striped unstackable={ true }>
  //         <Table.Body>
  //           { members.map(({ user, score }) => {
  //             return (
  //               <Table.Row key={ user.id }>
  //                 <Table.Cell>{ user.displayName }</Table.Cell>
  //                 <Table.Cell>{ score }</Table.Cell>
  //               </Table.Row>
  //             )
  //           })
  //           }
  //         </Table.Body>
  //       </Table>
  //
  //     </Card.Content>
  //   </Card>
  // );
};

export default withTeamScores(FB_COLLECTION_PREVIOUS_POSTED_CONTENT)(injectSheet(styles)(PeopleReward));
