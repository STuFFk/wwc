import TeamsScore from './TeamsScore';
import withTeamScores from '../hoc/withTeamScores';
import { FB_COLLECTION_CURRENT_POSTED_CONTENT } from '../../postedContent';

export default withTeamScores(FB_COLLECTION_CURRENT_POSTED_CONTENT)(TeamsScore);
