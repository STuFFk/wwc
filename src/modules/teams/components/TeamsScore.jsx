import React, { Component }  from 'react';
import { Card } from 'semantic-ui-react';

// import withTeamScores from '../hoc/withTeamScores';
// import { FB_COLLECTION_CURRENT_POSTED_CONTENT } from '../../postedContent';

class TeamsScore extends Component {
  constructor(props) {
    super(props);

    this.recharts = null;
    this.state = {
      ready: false,
    }
  }

  componentDidMount() {
    import('recharts').then(recharts => {
      this.recharts = recharts;
      this.setState(() => ({ ready: true }));
    });
  }

  render() {
    const { teamsScore, ignoreProductColumn } = this.props;

    if (!this.state.ready) {
      return null;
    }

    const { Cell, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } = this.recharts;

    const scores = teamsScore;

    if (ignoreProductColumn) {
      scores.pop();
    }

    return (
      <Card fluid raised>
        <Card.Content header='Teams Scores' />
        <Card.Content>
          {
            React.createElement(ResponsiveContainer, { width: '100%', height: 306 },
              React.createElement(BarChart, { data: scores, margin: {top: 0, right: 0, left: -20, bottom: 0} },
                [
                  React.createElement(XAxis, { key: 'a', dataKey: 'name' }),
                  React.createElement(YAxis, { key: 'b' }),
                  React.createElement(CartesianGrid, { key: 'c', strokeDasharray: '3 3' }),
                  React.createElement(Tooltip, { key: 'd' }),
                  React.createElement(Bar, { key: 'e', dataKey: 'score', fill: '#FFBB28' },
                    scores.map((entry, index) => (
                        React.createElement(Cell, { key: `cell-${index}`, fill: entry.color })
                      ))
                  ),
                ]
              )
            )
          }
        </Card.Content>
      </Card>
    );
  }
}

export default TeamsScore;
