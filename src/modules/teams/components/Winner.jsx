import React from 'react';
import sortBy from 'lodash/sortBy';
import injectSheet from 'react-jss'
import { Card, Table } from 'semantic-ui-react';

import { getWinningTeam } from '../../rewards';
import withTeamScores from '../hoc/withTeamScores';
import { FB_COLLECTION_PREVIOUS_POSTED_CONTENT } from '../../postedContent';
import { getUserById } from '..';

const styles = {
  root: { textAlign: 'center' },
  cup: {
    fontSize: '4rem',
    lineHeight: '4rem',
    marginBottom: '1rem',
  },
  title: {
    fontSize: '1.5rem',
  }
};

const Winner = ({ classes, teamsScore, usersScore }) => {
  const winner = getWinningTeam(teamsScore);

  if (!winner) {
    return null;
  }

  let members = winner.members.map((memberId) => {
    const user = getUserById(memberId);
    const userScore = usersScore[memberId] || { score : 0};
    return {
      user,
      score: userScore.score,
    }
  });

  members = sortBy(members, 'score').reverse();

  return (
    <Card fluid raised>
      <Card.Content header='Winner!' />
      <Card.Content className={ classes.root }>
          <div className={ classes.cup }><span role="img" aria-label="Cup">🏆</span></div>
          <div className={ classes.title }>Winner <strong style={ { color: winner.color } }>{winner.name}</strong> with <strong>{winner.score}</strong> points!</div>

        <Table celled striped unstackable={ true }>
          <Table.Body>
            { members.map(({ user, score }) => {
              return (
                <Table.Row key={ user.id }>
                  <Table.Cell>{ user.displayName }</Table.Cell>
                  <Table.Cell>{ score }</Table.Cell>
                </Table.Row>
              )
            })
            }
          </Table.Body>
        </Table>

      </Card.Content>
    </Card>
  );
};

export default withTeamScores(FB_COLLECTION_PREVIOUS_POSTED_CONTENT)(injectSheet(styles)(Winner));
