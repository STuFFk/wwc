import React from 'react';
import { Card } from 'semantic-ui-react';
import { Table, Label } from 'semantic-ui-react';

import { FB_COLLECTION_CURRENT_POSTED_CONTENT } from '../../postedContent';
import withTeamScores from '../hoc/withTeamScores';
import { getTeams, getUserById } from '../';

const TeamsMembers = ({ teamsScore, usersScore }) => {
  const teams = getTeams();
  const max = teams.reduce((acc, team) => {
    return Math.max(acc, team.members.length);
  }, 0);

  let list = new Array(teams.length).fill(null);
  list = list.map((_, n) => new Array(max).fill(null).map((_, m) => teams[n].members[m]));

  return (
    <Card fluid raised>
      <Card.Content header='Teams Members' />
      <Card.Content>
        <Table celled striped unstackable={ true }>
          <Table.Header>
            <Table.Row>
              {
                teams.map((team, n) => {
                  let score = null;
                  if (teamsScore.length > 0) {
                    score = teamsScore[n].score;
                  }

                  return (
                    <Table.HeaderCell key={ n } singleLine>
                      <Label color={ team.color }>{ team.name }</Label> { score ? `(${score})` : '' }
                    </Table.HeaderCell>
                  )
                })
              }
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {
              new Array(max).fill(null).map((_, n) => (
                <Table.Row key={ n }>
                  {
                    list.map((team, m) => {
                      const user = getUserById(list[m][n]) || {};
                      const userScore = usersScore && usersScore[user.id] ? usersScore[user.id].score : null;

                      return <Table.Cell key={ `${n}-${m}_${user.id}` }>{ user.displayName } { userScore ? <strong>({ userScore })</strong> : '' }</Table.Cell>
                    })
                  }
                </Table.Row>
              ))
            }
          </Table.Body>
        </Table>
      </Card.Content>
    </Card>
  );
};

export default withTeamScores(FB_COLLECTION_CURRENT_POSTED_CONTENT)(TeamsMembers);
