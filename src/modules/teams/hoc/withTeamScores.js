import React, { Component } from 'react';
import map from 'lodash/map';

import { getTeamForUserId, getTeams, getUserById, getUsers } from '../';
import { getTypeFromId } from '../../postedContent';

const withTeamScores = (firebaseCollectionName) => (WrappedComponent) => {
  return class WithAuthorizedUser extends Component {
    static displayName = `WithTeamScores(${WrappedComponent.displayName || WrappedComponent.name})`;

    constructor(props) {
      super(props);

      this.state = {
        teamsScore: [],
        usersScore: null,
      };

      const db = window.firebase.firestore();

      this.unsubscribe = db.collection(firebaseCollectionName)
        .onSnapshot(querySnapshot => {
          const teams = getTeams().reduce((acc, team) => {
            acc[team.id] = { ...team, score: 0 };
            return acc;
          }, {});

          const users = {};

          querySnapshot.forEach(doc => {
            const data = doc.data();
            const { userId } = data;
            const team = getTeamForUserId(userId);
            const type = getTypeFromId(data.typeId);
            const user = getUserById(userId);

            if (!users[user.id]) {
              users[user.id] = {
                score: 0,
              }
            }

            users[user.id].score += type.points;
            teams[team.id].score += type.points;
          });

          const fullUserList = Object.values(getUsers()).reduce((list, user, k) => {
            list[user.id] = users[user.id] ? users[user.id] : { score: 0 };
            return list;
          }, {});

          this.setState(() => ({
            teamsScore: map(teams, (team) => team ),
            usersScore: fullUserList,
          }));
        });
    }

    componentWillUnmount() {
      this.unsubscribe();
    }

    render() {
      return <WrappedComponent usersScore={ this.state.usersScore } teamsScore={ this.state.teamsScore } {...this.props } />;
    }
  };
};

export default withTeamScores;
