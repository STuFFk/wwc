import React, { Component } from 'react';

import { PassedPostedContentList } from './modules/postedContent';
import { TeamsPreviousScore, Winner, PeopleReward } from './modules/teams';

class TabPassed extends Component {
  render() {
    return (
      <div>
        <div className="grid">
          <div className="col-6_sm-12">
            <Winner />
          </div>
          <div className="col-6_sm-12">
            <PeopleReward />
          </div>
          <div className="col-12_sm-12">
            <TeamsPreviousScore ignoreProductColumn />
          </div>
        </div>
        <PassedPostedContentList readOnly />
      </div>
    )
  }
}

export default TabPassed;
