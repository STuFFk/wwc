import React, { Component } from 'react';
import { Tab } from 'semantic-ui-react';
import { withRouter, Route } from 'react-router-dom';
import { AppBar } from './modules/appBar';
import './App.css';

import TabCurrent from './TabCurrent';
import TabPassed from './TabPassed';

class App extends Component {

  handleTabChange = (event, data) => {
    if (data.activeIndex === 1) {
      this.props.history.push('/previous-results');
    } else {
      this.props.history.push('/');
    }
  };

  render() {
    return (
      <div>
        <div className="app">
          <AppBar />

          <Route children={({ location }) => {
            const defaultActiveIndex = location.pathname === '/previous-results' ? 1 : 0;

            return (
              <Tab defaultActiveIndex={ defaultActiveIndex } onTabChange={ this.handleTabChange } panes={[
                { menuItem: 'Scores', render: () =>  <Tab.Pane><TabCurrent /></Tab.Pane> },
                { menuItem: 'Previous Results', render: () => <Tab.Pane><TabPassed /></Tab.Pane>},
              ]} />
            )
          }} />
        </div>

      </div>
    );
  }
}

export default withRouter(App);
