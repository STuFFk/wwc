import React, { Component } from 'react';
import { Confirm } from 'semantic-ui-react';

import { withAuthorizedUser } from './modules/auth';
import { TeamsCurrentScore, TeamsMembers } from './modules/teams';
import { CurrentPostedContentList, AddPostedContentFormModal } from './modules/postedContent';
import { addContent, deleteContent } from './modules/postedContent';

class TabCurrent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAddModalOpened: false,
      isConfirmDeleteOpened: false,
      selectedContent: null,
    }
  }
  addModalSwitch = () => {
    this.setState(state => {
      return {
        isAddModalOpened: !state.isAddModalOpened,
      }
    });
  };

  handleSubmit = async ({ userId, typeId, url }) => {
    await addContent(userId, typeId, url);
  };

  confirmDeleteSwitch = (selectedContent) => {
    this.setState(state => {
      return {
        isConfirmDeleteOpened: !state.isConfirmDeleteOpened,
        selectedContent,
      }
    });
  };

  handleDelete = () => {
    deleteContent(this.state.selectedContent.id);

    this.setState(state => {
      return {
        isConfirmDeleteOpened: false,
        selectedContent: null,
      }
    });
  };

  render() {
    const { user } = this.props;

    return (
      <div>
        <div className="grid">
          <div className="col-6_sm-12">
            <TeamsCurrentScore/>
          </div>
          <div className="col-6_sm-12">
            <TeamsMembers/>
          </div>
        </div>

        <AddPostedContentFormModal
          isOpened={this.state.isAddModalOpened}
          onClose={this.addModalSwitch}
          onSubmit={this.handleSubmit}
        />

        <Confirm
          open={this.state.isConfirmDeleteOpened}
          onCancel={this.confirmDeleteSwitch}
          onConfirm={this.handleDelete}
        />

        <CurrentPostedContentList readOnly={!user} onAdd={this.addModalSwitch} onDelete={this.confirmDeleteSwitch}/>
      </div>
      )
  }
}

export default withAuthorizedUser(TabCurrent);
