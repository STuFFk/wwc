import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { BrowserRouter as Router } from 'react-router-dom';

import './index.css';
import { fetchUsers, setUsers } from './modules/teams';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

injectTapEventPlugin();

window.firebase.initializeApp({
  "apiKey": process.env.REACT_APP_FIREBASE_APIKEY,
  "authDomain": process.env.REACT_APP_FIREBASE_AUTHDOMAIN,
  "databaseURL": process.env.REACT_APP_FIREBASE_DATABASEURL,
  "projectId": process.env.REACT_APP_FIREBASE_PROJECTID,
  "storageBucket": "",
  "messagingSenderId": process.env.REACT_APP_FIREBASE_MESSAGINSENDERID,
});

registerServiceWorker();

fetchUsers().then(users => {
  setUsers(users);
  ReactDOM.render(<Router><App /></Router>, document.getElementById('root'));
});



