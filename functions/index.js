const functions = require('firebase-functions');
const admin = require('firebase-admin');

const postedContentCollection = 'postedContent2';

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

const getUsers = () => {
  return db
    .collection('users')
    .orderBy('email', 'asc')
    .get()
    .then((querySnapshot) => {
      return querySnapshot.docs.map(doc => {
        const { email } = doc.data();
        return {
          id: +doc.id,
          email,
        }
      });
    });
};

const isPostedContentExists = (url) => {
  return db
    .collection(postedContentCollection)
    .where('url', '==', url)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.length !== 0;
    });
};

exports.addSlackPost = functions.https.onRequest((req, res) => {
  Promise.all([isPostedContentExists(req.body.postUrl), getUsers()]).then((values) => {
    const contentExists = values[0];
    const users = values[1];

    if (contentExists) {
      res.status(409).send('This posted content already exists!');
      return;
    }

    const user = users.find((user) => user.email === req.body.userEmail);

    if (!user) {
      res.status(404).send('User Not found');
      return;
    }

    db.collection(postedContentCollection).add({
      userId: user.id,
      typeId: 1,
      url: req.body.postUrl,
      createdAt: Date.now(req.body.createdAt),
    }).then(() => {
      const data = Object.assign(req.body, { points: 1 });
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(data));
    });
  });
});
